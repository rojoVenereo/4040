<?php
require_once("config.php");
$obj = new panel($sqli);
$data= $obj->getavailability();
$units =$obj->getUnits();
$availableYears =$obj->getYears();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="Martin Arroyo" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Hotel Panel</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/starter-template.css" rel="stylesheet">
    <script src="css/jquery.js"></script>
  </head>

  <body>

    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
      <a class="navbar-brand" href="#">Hotel Panel</a>

      </nav>

    <div class="container">
    
    <div class="well">
    <div class="row">
        <div class="options col-sm-7"><img  class="img-responsive promo" alt="Cotagge IMG" src="http://monteverde.com.mx/wp-content/uploads/2013/02/destacadagrande.jpg"> </img></div>
        <div class="options col-sm-5">
            <h3>Resevar en </h3>
            <p class="help">Man bun tote bag helvetica taxidermy, paleo salvia bitters waistcoat ramps yuccie hexagon. Polaroid meh wolf prism selfies, chambray literally pitchfork taiyaki slow-carb taxidermy mumblecore tote bag.</p>
            <p>
                <label>Año<label>
                <select id="year" class="form-control">
                    <option value="2017" selected="selected">2017</option>
                    <option value="2018" >2018</option>
                </select>
            </p>
            <p>
            <label>Mes<label>
            <select id="month"  class="form-control">
                <option value="1" selected="selected">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
            </select>
            </p>
        </div>
    </div>
    </div>
      <div class="container">
        <h1>Disponibilidad</h1>
         <div class="dinamicContent"></div>
      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      
      <script type="text/javascript" >
        var stringDays = ["LUN", "MAR", "MIE","JUE","VIE","SAB","DOM"];
        var months30days = [1,3,5,7,8,10,12];
        
        $(document).ready(function() {
            
            
            year    = $( "#year option:selected" ).val();
            month   = $( "#month option:selected" ).val();
            units=JSON.parse('<?php echo $units; ?>');
            data=JSON.parse('<?php echo $data; ?>');
            availableYears=JSON.parse('<?php echo $availableYears; ?>');
            $("#month").change(function(){
                $(".dinamicContent").html("");
                 month   = $( "#month option:selected" ).val();
                 
                 run();
                });
            $("#year").change(function(){
                 $(".dinamicContent").html("");
                 year   = $( "#year option:selected" ).val();
                 run();
                });
            run();
            
        });
        
        function run() {
            showTables(units);
            daysBackground(units, data);
            dayOfWeek();
       
            $(".monthLabel").html(" para "+$( "#month option:selected" ).html()+", "+year);
            if (month == 2) {
                $(".feb").remove();
            }
            if (isInArray(months30days, month)== 0){
                $(".sho").remove(); 
            }
        }
        
        function isInArray(array, value) {
            result= 0;
            $.each(array, function(k,v) {
                if (value == v) {
                    result=1;
                }
            });
            return result;
        }
        
        function setYears(){
            optStr="";
            availableYears.each(function (p,yvalue) {
                if (p===0) {
                    optStr=+ '<option value="'+yvalue+'" selected="selected">'+yvalue+'</option>';
                }else{
                    optStr=+ '<option value="'+yvalue+'">'+yvalue+'</option>';
                }
            });
            $("#year").html(availableYears);

        }
        
        function showTables(data) {
            $.each(data, function(k,i) {

                tabler= createTable("table"+i, i);
                $(".dinamicContent").append(tabler);
            });
            
           
        }
        
        function createTable(idTable, unit){
            strHead='<h3>Cabaña '+unit+'</h3><h5 class="monthLabel"></h5><table id="calendar'+idTable+'" class="table tableSchedule table-condensed"><thead><tr><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th>18</th><th>19</th><th>20</th><th>21</th><th>22</th><th>23</th><th>24</th><th>25</th><th>26</th><th>27</th><th>28</th><th class="feb">29</th><th class="feb">30</th><th class="feb sho">31</th></tr></thead><tbody><tr><td class="d1"> </td><td class="d2"> </td><td class="d3"> </td><td class="d4"> </td><td class="d5"> </td><td class="d6"> </td><td class="d7"> </td><td class="d8"> </td><td class="d9"> </td><td class="d10"> </td><td class="d11"> </td><td class="d12"> </td><td class="d13"> </td><td class="d14"> </td><td class="d15"> </td><td class="d16"> </td><td class="d17"> </td><td class="d18"> </td><td class="d19"> </td><td class="d20"> </td><td class="d21"> </td><td class="d22"> </td><td class="d23"> </td><td class="d24"> </td><td class="d25"> </td><td class="d26"> </td><td class="d27"> </td><td class="d28 feb"> </td><td class="d29 feb"> </td><td class="d30"> </td><td class="d31 sho"></td></tr></tbody></table>';
            return strHead;
            
        }
        
        function daysBackground(units, data) {
            $.each(units, function(k,i) {
               if (typeof data[year][month] !== 'undefined'){
                 schedule = data[year][month][i];
                    $.each(schedule, function(key,value) { 
                        $("#calendartable"+i+" tbody tr ." +key).addClass("color"+value); 
                    });
                }
               
            });
        }
        
        function dayOfWeek() {
           $(".table thead tr").each(function (index) {
            day= $(this).children("th");
            day.each(function(r,d){
                month   = $( "#month option:selected" ).val();
                dayNumber = $(this).html();
                
                z = new Date(year, month -1, r);
              
                dow = z.getDay();
                $(this).append('<span class="dow">' +stringDays[dow]+'</span>');
                });
           });
        }
        
        
        
      </script>
  </body>
</html>