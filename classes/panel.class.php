<?php
	/*
	 * Clase que sirve para hacer la conexion a la base de datos, realizar consultas y obtener los resultados de la consulta
	 * usando mysqli
	*/	 
	class panel{
	
		private $sql;
		private $availability;
		protected $_getters = array('availability','sql');
		protected $_setters = array('availability','sql');

		function __construct($SQLobj){
         	$this->sql=$SQLobj;
	    }

		final public function __set($property, $value) {	//Magic setter
			if (in_array($property, $this->_setters)) {
				$this->$property = $value;
			}
			elseif (method_exists($this, '_set_' . $property))
				call_user_func(array($this, '_set_' . $property), $value);
			elseif (in_array($property, $this->_getters) || method_exists($this, '_get_' . $property))
				throw new Exception('La propiedad "' . $property . '" es de solo lectura.');
			else
				throw new Exception('La propiedad "' . $property . '" no es accesible.');
    	}

		final public function __get($property) {			//Magic getter
			if (in_array($property, $this->_getters)){
				return $this->$property;
			}elseif(method_exists($this, '_get_' . $property))
				return call_user_func(array($this, '_get_' . $property));
			elseif(in_array($property, $this->_setters) || method_exists($this, '_set_' . $property))
				throw new Exception('La propiedad "' . $property . '" es de solo escritura.');
			else
				throw new Exception('La propiedad  "' . $property . '" no es accesible.');
		}
		
		
		
		
		
		function getavailability(){
			$sqlString="select * from availability";
			$this->sql->query($sqlString);
			while($r=$this->sql->fetch()){
				//var_dump($r);
				$this->availability[$r["year"]] [$r["month"]] [$r["unit_id"]]= $r;
			}	
		return json_encode($this->availability);
		}
		
		function getUnits(){
			$sqlString= "SELECT unit_id as unit FROM availability GROUP BY unit_id";
			$this->sql->query($sqlString);
			$pila = array();
			while($r=$this->sql->fetch()){
				//var_dump($r);
				array_push($pila, $r["unit"]);
			}
			return json_encode($pila);
		}
		
		function getYears(){
			$sqlString= "SELECT year  FROM availability GROUP BY year";
			$this->sql->query($sqlString);
			$pila = array();
			while($r=$this->sql->fetch()){
				array_push($pila, $r["year"]);
			}
			return json_encode($pila);
		}
	}
?>
